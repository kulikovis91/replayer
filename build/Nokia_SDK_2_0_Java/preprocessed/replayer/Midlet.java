/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package replayer;

import java.io.InputStream;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;
import javax.microedition.midlet.*;

/**
 * @author Ivan
 */
public class Midlet extends MIDlet {

    Player player;
    public void startApp() {        
        try {
            InputStream stream = getClass().getResourceAsStream("res/gintama.mp3");
            player = Manager.createPlayer(stream, "audio/mp3");
            player.prefetch();
            player.realize();
            player.start();
        }
        catch (Exception e) { }
    }
    
    public void pauseApp() {
    }
    
    public void destroyApp(boolean unconditional) {
    }
}
