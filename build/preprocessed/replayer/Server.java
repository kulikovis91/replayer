/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package replayer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import javax.microedition.io.Connector;
import javax.microedition.io.ServerSocketConnection;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.file.FileConnection;
import javax.microedition.io.file.FileSystemRegistry;
import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.control.VolumeControl;
import replayer.Utils.Logger;

/**
 *
 * @author Ivan
 */
public class Server implements Runnable {

    public InputStream is = null;
    public OutputStream os = null;
    String filesystemPrefix = "file:///";
    String currentDir = null;
    javax.microedition.media.Player p;
    int vol = 50;

    public void Reply(String msg) {
        try {
            os.write(msg.getBytes());
            os.write("\n\r".getBytes());
        } catch (IOException e) {
            Logger.log(e.getMessage());
        }
    }

    private void getFileList(String path) {

        Enumeration roots = FileSystemRegistry.listRoots();
        try {
            if (path.startsWith(filesystemPrefix)) {
                while (roots.hasMoreElements()) {
                    if (path.startsWith(filesystemPrefix + roots.nextElement().toString())) {
                        Logger.log("Enumerating " + path + "\n\r");
                        //Opens a file connection in READ mode
                        FileConnection fc = (FileConnection) Connector.open(path, Connector.READ);
                        Logger.log("File connection opened.");
                        Enumeration filelist = fc.list("*", true); //also hidden files are shown
                        Logger.log("File list retrieved.");
                        String filename;

                        while (filelist.hasMoreElements()) {
                            filename = (String) filelist.nextElement();
                            Reply(filename);
                            Logger.log(filename);
                        }
                        fc.close();
                    } else {
                        break;
                    }
                }
            } else {
                Reply("Invalid path format...");
                Logger.log("Invalid path format...");
            }
        } catch (IOException e) {
            Reply(e.getMessage());
            Logger.log(e.getMessage());
        }
    }

    public void run() {
        try {
            ServerSocketConnection ssc = (ServerSocketConnection) Connector.open("socket://:9002");
            StreamConnection sc = null;
            Logger.log("Listening at " + ssc.getLocalAddress() + ":" + ssc.getLocalPort());

            try {
                sc = ssc.acceptAndOpen();
                is = sc.openInputStream();
                os = sc.openOutputStream();
                os.write(("Hello! I'm " + System.getProperty("microedition.platform") + "\n\r").getBytes());


                while (true) {
                    os.write(">".getBytes());
                    int ch = 0;
                    StringBuffer sb = new StringBuffer();
                    while ((ch = is.read()) != -1 && ch != (int) '\n') {
                        sb.append((char) ch);
                    }
                    String tmp = sb.toString().trim();
                    if (tmp.startsWith("play")) {
                        String path = tmp.substring(tmp.indexOf(" ") + 1);
                        PlaySound(path);
                    } else if (tmp.equals("roots")) {
                        Enumeration roots = FileSystemRegistry.listRoots();
                        while (roots.hasMoreElements()) {
                            os.write(roots.nextElement().toString().getBytes());
                            os.write("\n\r".getBytes());
                        }
                    } else if (tmp.startsWith("ls")) {
                        String path = tmp.substring(tmp.indexOf(" ") + 1);
                        getFileList(path);
                    } else if (tmp.equals("stop")) {
                        Stop();
                    } else if (tmp.equals("up")) {
                        VolumeUp();
                    } else if (tmp.equals("down")) {
                        VolumeDown();
                    } else if (tmp.equals("start")) {
                        Play();
                    }
                }
            } catch (Exception e) {
                ssc.close();
                sc.close();
                is.close();
                os.close();
            }

        } catch (IOException x) {
        }
    }

    public void PlaySound(String path) {
        try {
            Logger.log("Searching file: " + path);
            InputStream stream = ((FileConnection) Connector.open(path, Connector.READ)).openDataInputStream();
            p = Manager.createPlayer(stream, "audio/mp3");
            //Logger.log("Fetching...");
            //p.prefetch();
            Logger.log("Now playing...");
            p.start();
        } catch (Exception e) {
            Logger.log("Caught exception: " + e.getMessage());
        }
    }

    public void Stop() {
        try {
            Logger.log("Stopping playback...");
            p.stop();
        } catch (MediaException e) {
            Logger.log("Caught exception: " + e.getMessage());
        }
    }

    public void Play() {
        try {
            Logger.log("Resuming playback...");
            p.start();
        } catch (MediaException e) {
            Logger.log("Caught exception: " + e.getMessage());
        }
    }
    
    public void VolumeUp() {
        Logger.log("VolumeUp...");
        VolumeControl control = (VolumeControl) p.getControl("VolumeControl");
        if (control != null) {
            vol += 10;
            control.setLevel(vol);
        }
    }

    public void VolumeDown() {
        Logger.log("VolumeDown...");
        VolumeControl control = (VolumeControl) p.getControl("VolumeControl");
        if (control != null) {
            vol -= 10;
            control.setLevel(vol);
        }
    }

    public static void start(Midlet parent) {
        (new Thread(new Server())).start();
    }
}
