/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package replayer;

import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Form;

/**
 *
 * @author Ivan
 */
public class Utils {
    static class Logger {
        private static Form form1;

        public static void init(Midlet parent) {
            form1 = new Form("RePlayer");
            Display disp = Display.getDisplay(parent);
            disp.setCurrent(form1);
        }
        
        public static void log(String msg) {
            //form1.append(msg + "\n");
            form1.append(msg);
        }
    }
}
