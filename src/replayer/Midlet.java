/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package replayer;

import javax.microedition.midlet.*;
import replayer.Utils.Logger;

/**
 * @author Ivan
 */
public class Midlet extends MIDlet {
    Server srv = new Server();
    public void startApp() {
        Logger.init(this);
        Logger.log("Application started...");
        srv.start(this);
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
        
    }
}
